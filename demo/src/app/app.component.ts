import { Component } from '@angular/core';
import * as L from 'leaflet';
import 'leaflet-notifications';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

    private notifications = {
        default: null,
        modern: null,
        pastel: null
    }

    public selectedPosition: "topright"|"topleft"|"bottomright"|"bottomleft" = "topright";
  
  
    ngOnInit(){
      let mymap = L.map('map', { zoomControl: false }).setView([51.505, -0.09], 4);
      L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(mymap);
      this.notifications.default = L.control['notifications']().addTo(mymap);
      this.notifications.pastel = L.control['notifications']({className: "pastel"}).addTo(mymap);
      this.notifications.modern = L.control['notifications']({className: "modern"}).addTo(mymap);
    }

    public add(style: string, type: string){
        if(this.selectedPosition !==  this.notifications[style].getPosition()) {
            this.notifications[style].setPosition(this.selectedPosition);
        }
        const isClosable = document.getElementById('closableCheckbox')['checked'];
        const isDismissable = document.getElementById('dismissableCheckbox')['checked'];

        this.notifications[style][type](type.toUpperCase(), `This is a ${type} message`, {
            closable: isClosable,
            dismissable: isDismissable
        });
    }



}
